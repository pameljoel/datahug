[![Netlify Status](https://api.netlify.com/api/v1/badges/a252eb07-e97c-4360-a78c-363d81d04ec0/deploy-status)](https://app.netlify.com/sites/gallant-brahmagupta-7a7caa/deploys)

# commands
This is a very barebones repo for the datahug exercise, in order to see it correctly you will need to build css.

## nvm use 10
This repo uses version 10 of node

## npm run build-css
this is the command used to minify scss into css.

## npm run watch-css
this is the command used in development to work with CSS

## view project
After you bild the css you just need to open the index.html file.